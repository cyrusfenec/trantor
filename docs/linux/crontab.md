# Administración de los *jobs* del Cron



```
# Listar tareas exitentes del usuario
$ crontab -l

# Editar archivo crontab del usuario
$ crontab -e
```

_**Ejemplos**_

___

_crontab_

```
#00 21 * * * /bashbackup/dailydb.sh 
#00 22 * * 1-5 /bashbackup/dailyfiles.sh
@weekly /home/wss1t3/scripts/backup_databases.sh
```

__script for backups DB_

```
#!/bin/bash

# Database credentials
user="backup_agent_user"
password="ej%Un4gnt5%hHK21"
host="localhost"
#db_name="strepuve"

# Other options
backup_path="/var/www/db_backup/"
date=$(date +"%d_%m_%Y_%d_%H_%M_%S")

# Set default file permissions
umask 177

# Dump database into SQL file
#mysqldump --user=$user --password=$password --host=$host -- > $backup_path/$db_name-$date.sql
mysqldump --user=$user --password=$password strepuve > $backup_path/strepuve-$date.sql
mysqldump --user=$user --password=$password dby2stemain > $backup_path/dby2stemain-$date.sql
mysqldump --user=$user --password=$password siteseisp > $backup_path/siteseisp-$date.sql



# Delete files older than 30 days
find $backup_path/* -mtime +30 -exec rm {} \;
```

_script for backup files_

```
#!/bin/bash 
date >> /var/www/changelog.md 
#rsync -av --delete /var/www/ /hdbackup/files_sites/ 
rsync -a --delete /var/www/ /hdbackup/files_sites/
```


