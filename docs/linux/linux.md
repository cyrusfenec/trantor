# Linux - Utilities

#### Change Timezone

```
# set new timezone
$ sudo timedatectl set-timezone America/Mexico_City

#To verify the change
$ timedatectl
```

#### SSH sin password

```
# Maquina cliente
$ ssh-keygen -b 4096 -t rsa
$ ssh-copy-id remoteuser@remote_host
# Ingreso a remoto
$ ssh remoteuser@remote_host
```

#### Get Mac from IP

```
 $ arp -a | grep '192.168.5.92'
```





